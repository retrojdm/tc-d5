# Sony TC-D5 idlers
Dimensions are given in mm as: inner diameter x thickness x width

## X-3556-224-0 | Flywheel Ass'y
- 24 x 5 x 1 (with 0.8mm incline from outer to inner diameter)

## X-3545-521-0 | Pully Ass'y, forward
- This has a T-shaped profile
- Main section: 9.75 x 2 x 2.85
- Inside section: 8.25 x 0.75 x 0.55

## X-3556-216-0 | Idler Ass'y, fast forward
- There are two idlers on the same plastic shaft/assembly
- Large: 14.3 x 1.85 x 1.45
- Small: 5.5 x 2 x 2.5

## X-3556-217-0 | Pulley Ass'y, rewind
- 14.3 x 1.85 x 1.45

## 3-545-601-00 | Belt, pulley

## 3-507-115-00 | Belt (A)

## 3-545-602-00 | Belt, tape counter

## X-3545-548-0 | Lever Ass'y, pinchroller
